import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AudioAnalyzerService {
  private _callbackId: number = null  // we only allow scheduling when this is null
  private _timestampCurrent: DOMHighResTimeStamp = null
  private _timestampLast: DOMHighResTimeStamp = null
  private _audioContext: AudioContext = new AudioContext()
  private _inputStreamNode: MediaStreamAudioSourceNode = null
  private _analyzerNode: AnalyserNode = null

  private _cycle: BehaviorSubject<number> = new BehaviorSubject<number>(0)
  readonly cycle$: Observable<number> = this._cycle.asObservable()
  get cycle(): number {
    return this._cycle.getValue()
  }

  private _isRunning: BehaviorSubject<boolean> = new BehaviorSubject(false)
  readonly isRunning$: Observable<boolean> = this._isRunning.asObservable()
  get isRunning(): boolean {
    return this._isRunning.getValue()
  }

  get fps(): number {
    if (this._timestampLast === null || this._timestampCurrent === null) {
      return null
    }
    return 1000. / (this._timestampCurrent - this._timestampLast)
  }

  constructor() {
  }

  // --------------------
  // PUBLIC INTERFACES
  // --------------------
  start() {
    // sanity check in case the analyzer is already running
    if (this.isRunning) {
      return
    }

    console.log('analyzer starting')
    this._initAudioContext().then(() => console.log('AudioContext init finished'))

    // reset timestamps to avoid weird values in the fps calculations
    this._timestampCurrent = null
    this._timestampLast = null

    this._isRunning.next(true)
    this._queueNextCycle()
  }

  stop() {
    console.log('analyzer stopping')
    if (this._callbackId !== null) {
      window.cancelAnimationFrame(this._callbackId)
      this._callbackId = null
    }
    this._isRunning.next(false)
  }

  runToggle() {
    if (this._isRunning.getValue() === undefined) {
      console.log('an error happened. analyzer state change blocked.')
    } else if (this._isRunning.getValue()) {
      this.stop()
    } else {
      this.start()
    }
  }

  /**
   * Locks the analyzer. Use in cases of unrecoverable error to avoid wasting CPU power.
   */
  lock() {
    this.stop()

    console.log('analyzer locking')
    this._isRunning.next(undefined)
    this._callbackId = -1
  }

  // --------------------
  // UTILITY FUNCTIONS
  // --------------------
  /**
   * Queue the next cycle. Does not check if on is already queued!
   *
   * @param timeout - in ms. wait a certain amount of time before scheduling the next cycle.
   */
  private _queueNextCycle(timeout?: number) {
    if (this.isRunning && this._callbackId === null) {
      if (timeout) {
        setTimeout(() => {
          this._queueNextCycle()
        }, timeout)
        return
      }
      this._callbackId = window.requestAnimationFrame(this._runSingleCycle.bind(this))
    }
  }

  private async _initAudioContext(): Promise<void> {
    // skip init if it already happened
    if (this._inputStreamNode !== null) {
      return
    }

    try {
      // get microphone input as an InputStreamNode
      const mediaStream = await navigator.mediaDevices.getUserMedia({audio: true, video: false})
      this._inputStreamNode = this._audioContext.createMediaStreamSource(mediaStream)

      // connect other AudioNodes
      this._analyzerNode = this._audioContext.createAnalyser()
      this._inputStreamNode.connect(this._analyzerNode)
      this._analyzerNode.connect(this._audioContext.destination)
    } catch (e) {
      console.log('error accessing the input stream: ' + e.toString())  // TODO: add error message visible to normal users

      // clean up the audio nodes
      try {
        this._inputStreamNode.disconnect()
      } finally {
        this._inputStreamNode = null
      }
      try {
        this._analyzerNode.disconnect()
      } finally {
        this._analyzerNode = null
      }

      this.lock()
    }
  }

  // --------------------
  // INTERNAL LOGIC
  // --------------------
  /**
   * Function for the main loop of the analyzer. Repeatedly called by the browsers scheduler through callbacks.
   * DO NOT CALL THIS DIRECTLY! This should only be called as a callback by the browsers scheduler (through the requestAnimationFrame
   * mechanism) and requires the analyzers this variable bound to it.
   *
   * @param timestamp - timestamp passed by the browser when calling the function
   */
  private _runSingleCycle(timestamp: DOMHighResTimeStamp) {
    // preparations for the cycle
    this._timestampLast = this._timestampCurrent
    this._timestampCurrent = timestamp
    this._cycle.next(this.cycle + 1)
    this._callbackId = null  // reset callback id to allow scheduling of another cycle

    // skip the rest and wait a bit if the input stream is not (yet) available
    if (this._inputStreamNode === null) {
      this._queueNextCycle(200)
      return
    }

    // TODO: analyze stuff

    // queue the function again if the analyzer is still supposed to be active
    this._queueNextCycle()
  }

}
