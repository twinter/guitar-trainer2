import { TestBed } from '@angular/core/testing';

import { AudioAnalyzerService } from './audio-analyzer.service';

describe('AudioAnalyzerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AudioAnalyzerService = TestBed.get(AudioAnalyzerService);
    expect(service).toBeTruthy();
  });
});
