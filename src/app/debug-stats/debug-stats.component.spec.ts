import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DebugStatsComponent } from './debug-stats.component';

describe('DebugStatsComponent', () => {
  let component: DebugStatsComponent;
  let fixture: ComponentFixture<DebugStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DebugStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DebugStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
