import { Component, OnInit } from '@angular/core'
import { AudioAnalyzerService } from '../audio-analyzer.service'

@Component({
  selector: 'app-debug-stats',
  templateUrl: './debug-stats.component.html',
  styleUrls: ['./debug-stats.component.scss']
})
export class DebugStatsComponent implements OnInit {
  fps: string = 'n/a'
  rms: string = '[rms]'

  constructor(private analyzer: AudioAnalyzerService) {
  }

  ngOnInit() {
    this.analyzer.cycle$.subscribe(value => {
      this.setFPS()
    })
  }

  setFPS() {
    const fps = this.analyzer.fps
    if (fps === null) {
      this.fps = 'n/a'
    } else {
      this.fps = fps.toFixed(2).toString()
    }
  }

}
