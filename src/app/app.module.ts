import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap'

import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component'
import { TrainerComponent } from './trainer/trainer.component'
import { AnalyzerResultsComponent } from './analyzer-results/analyzer-results.component'
import { RecorderComponent } from './recorder/recorder.component';
import { DebugStatsComponent } from './debug-stats/debug-stats.component';
import { AnalyzerRunToggleComponent } from './analyzer-run-toggle/analyzer-run-toggle.component'

@NgModule({
  declarations: [
    AppComponent,
    TrainerComponent,
    AnalyzerResultsComponent,
    RecorderComponent,
    DebugStatsComponent,
    AnalyzerRunToggleComponent
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
