import { async, ComponentFixture, TestBed } from '@angular/core/testing'

import { AnalyzerResultsComponent } from './analyzer-results.component'

describe('AnalyzerComponent', () => {
  let component: AnalyzerResultsComponent
  let fixture: ComponentFixture<AnalyzerResultsComponent>

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AnalyzerResultsComponent]
    })
      .compileComponents()
  }))

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzerResultsComponent)
    component = fixture.componentInstance
    fixture.detectChanges()
  })

  it('should create', () => {
    expect(component).toBeTruthy()
  })
})
