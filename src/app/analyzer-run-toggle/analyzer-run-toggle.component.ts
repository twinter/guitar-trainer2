import { Component, Input, OnInit } from '@angular/core'
import { AudioAnalyzerService } from '../audio-analyzer.service'

@Component({
  selector: 'app-analyzer-run-toggle',
  template:
      `<button class="{{ cssClasses }}" (click)="this.analyzer.runToggle()">{{ this.text }}</button>`,
  styles: []
})
export class AnalyzerRunToggleComponent implements OnInit {
  @Input() cssClasses: string

  private text: string = 'run'

  constructor(private analyzer: AudioAnalyzerService) {
  }

  ngOnInit() {
    this.setText(this.analyzer.isRunning)
    this.analyzer.isRunning$.subscribe(value => this.setText(value))
  }

  setText(status: boolean) {
    if (status === undefined) {
      this.text = 'error!'
    } else if (status) {
      this.text = 'stop'
    } else {
      this.text = 'run'
    }
  }

}
