import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnalyzerRunToggleComponent } from './analyzer-run-toggle.component';

describe('AnalyzerRunToggleComponent', () => {
  let component: AnalyzerRunToggleComponent;
  let fixture: ComponentFixture<AnalyzerRunToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnalyzerRunToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnalyzerRunToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
